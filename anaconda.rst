Anaconda
========

Frequently used command
~~~~~~~~~~~~~~~~~~~~~~~
In the GPU cluster, you may need to provide the full path in order to use anaconda.

1. List all the existing environments: ``anaconda3/bin/conda info --envs``

2. Activate tensorflow environments: ``source anaconda3/bin/activate tensorflow``

3. To activate this environment, use ``conda activate py27``,

4. To deactivate an active environment, use ``conda deactivate``.

Install venv
~~~~~~~~~~~~
Refer to conda.io:

`Installing a different version of python <https://conda.io/docs/user-guide/tasks/manage-python.html#installing-a-different-version-of-python>`_

