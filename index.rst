.. coding notes documentation master file, created by
   sphinx-quickstart on Fri Aug 25 20:05:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to My Coding Notes
==========================
This is a collection of *scattered pieces of coding notes* to
serve as memos. I share them with the hope that they *might* help
more *software engineers* or *programmers*.

.. code-block:: python

   from pharrell.coding_experience import i_will_update_coding_notes

   if i_have_something_new and i_happened_to_have_some_time:
      i_will_update_coding_notes()

You can type anything that you want in the search box. If my coding notes
happened to have something related, it will come up on the screen.

My Coding Notes is organized into a couple sections:

* :ref:`artificial-intelligence`.
* :ref:`win`
* :ref:`unix`
* :ref:`web-dev`
* :ref:`write-the-docs`
* :ref:`lang`


.. _artificial-intelligence:

.. toctree::
   :maxdepth: 2
   :caption: AI

   deep-learning
   tensorflow


.. _win:

.. toctree::
   :maxdepth: 2
   :caption: Windows

   windows


.. _unix:

.. toctree::
   :maxdepth: 2
   :caption: Unix

   docker
   mac
   ubuntu
   ssh
   zsh-shell
   supervisor
   ports
   git
   anaconda
   vim
   htcondor
   opencv

.. _web-dev:

.. toctree::
   :maxdepth: 2
   :caption: Web Dev

   frontend-node
   frontend-npm
   frontend-react-redux
   backend-django
   backend-drf
   backend-psql
   backend-dot
   nginx
   http
   ssl
   internet


.. _write-the-docs:

.. toctree::
   :maxdepth: 2
   :caption: Write the Docs

   sphinx-get-started
   sphinx-how-to
   markdown
   gitbook
   mkdocs


.. _lang:

.. toctree::
   :maxdepth: 2
   :caption: Coding Languages

   python

------------------------------------------------------------------------------

How to run the notes
====================

.. include:: ./README.rst

